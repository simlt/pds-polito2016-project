﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;


namespace clientWPF
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<RemoteServer> serverList = new ObservableCollection<RemoteServer>();
        public ObservableCollection<RemoteServer> Servers { get { return serverList; } }
        Timer updateFocusViewTimer = new Timer(400);
        private RemoteServer _selectedServer;
        public RemoteServer SelectedServer {
            get { return _selectedServer; }
            set
            {
                if (value != _selectedServer)
                {
                    _selectedServer = value;
                    focusApp.DataContext = _selectedServer;
                }
            }
        }

        //elementi utili all'ordinamento della listview
        GridViewColumnHeader _lastHeaderClicked = null;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;

        //salvo il keystroke da inviare come variabile della classe per comodita'
        keystroke messageToSend = new keystroke();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            focusApp.DataContext = SelectedServer;
        }

        // Return true if connection is successful
        internal bool addServer(string IP, string port)
        {
            var remote = new RemoteServer(IP, port);
            if (!remote.Connected)
            {
                remote.Close(null);
                //connessione non riuscita
                MessageBox.Show(this, "Errore di connessione, riprovare più tardi", "Errore di connessione", MessageBoxButton.OK);
                return false;
            }
            else
            {
                Servers.Add(remote);
                remote.RemoteClosed += Remote_RemoteClosed;
            }
            return true;
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            var window = new WindowNewServer();
            window.DataContext = this;
            window.Owner = this;
            window.Show();
        }

        private void buttonRemove_Click(object sender, RoutedEventArgs e)
        {
            var remote = SelectedServer;
            if (remote != null)
            {
                remote.Close(null);
                Servers.Remove(remote);
            }
        }

        private void Remote_RemoteClosed(object sender, string errorMsg)
        {
            var remote = (RemoteServer)sender;
            MessageBox.Show(this, String.Format("Errore sul server remoto \"{0:C}\": {1:C}", remote.ServerName, errorMsg));
            Servers.Remove(remote);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            foreach (var remote in Servers)
            {
                // Close all tcp connections to remotes
                remote.Close(null);
            }
        }

        private void listServer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedServer = (RemoteServer)listServer.SelectedValue;
            if (SelectedServer != null)
            {
                listViewApps.ItemsSource = SelectedServer.applicationList;
                //focusApp.DataContext = CurrentAppInFocus;

                //show input grid
                focusApp.IsEnabled = true;
                //only for focus percentage visualized
                updateFocusViewTimer.AutoReset = true;
                // TODO se si cambia server il vecchio server continua ad essere aggiornato ugualmente. Rimuovere l'hook prima di cambiare?
                updateFocusViewTimer.Elapsed += SelectedServer.RefreshFocusPercentageEvent;
                updateFocusViewTimer.Enabled = true;
            }
            else
            {
                listViewApps.ItemsSource = null;
                focusApp.IsEnabled = false;
            }
        }

        void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked != null)
            {
                if (headerClicked.Role != GridViewColumnHeaderRole.Padding)
                {
                    if (headerClicked != _lastHeaderClicked)
                    {
                        direction = ListSortDirection.Ascending;
                    }
                    else
                    {
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            direction = ListSortDirection.Descending;
                        }
                        else
                        {
                            direction = ListSortDirection.Ascending;
                        }
                    }

                    var binding = (Binding)headerClicked.Column.DisplayMemberBinding;
                    if (binding == null)
                        return;
                    string header = binding.Path.Path as string;

                    Sort(header, direction);

                    if (direction == ListSortDirection.Ascending)
                        headerClicked.Column.HeaderTemplate = Resources["HeaderTemplateArrowUp"] as DataTemplate;
                    else
                        headerClicked.Column.HeaderTemplate = Resources["HeaderTemplateArrowDown"] as DataTemplate;

                    // Remove arrow from previously sorted header
                    if (_lastHeaderClicked != null && _lastHeaderClicked != headerClicked)
                        _lastHeaderClicked.Column.HeaderTemplate = null;


                    _lastHeaderClicked = headerClicked;
                    _lastDirection = direction;
                }
            }
        }

        private void Sort(string sortBy, ListSortDirection direction)
        {
            CollectionView dataView = (CollectionView) CollectionViewSource.GetDefaultView(listViewApps.ItemsSource);

            if (dataView != null)
            {
                dataView.SortDescriptions.Clear();
                SortDescription sd = new SortDescription(sortBy, direction);
                dataView.SortDescriptions.Add(sd);
                dataView.Refresh();
            }
        }

        private void SendCommand_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)SendCommandFocusApp.IsChecked)
                sendCommandOnFocusApp();
            else if ((bool)SendCommandSelectedApp.IsChecked)
                sendCommandToAll();
            else
                MessageBox.Show(this, "Selezionare a chi si ha intenzione di inviare il comando", "Selezionare un opzione", MessageBoxButton.OK);
        }

        private void sendCommandOnFocusApp()
        {
            var remote = (RemoteServer)listServer.SelectedValue;
            if (remote != null)
            { 
                messageToSend.mods = ReadModifiers();
                messageToSend.ksAppID = 0;
                remote.sendCommand(messageToSend);
            }
        }

        private modifiers ReadModifiers()
        {
            modifiers modifiers = 0;
            if ((bool)CTRL.IsChecked)
                modifiers |= getModifier((string)CTRL.Content);
            if ((bool)ALT.IsChecked)
                modifiers |= getModifier((string)ALT.Content);
            if ((bool)SHIFT.IsChecked)
                modifiers |= getModifier((string)SHIFT.Content);

            return modifiers;
        }

        private modifiers getModifier(string name)
        {
            modifiers m;
            switch (name)
            {
                case "SHIFT":
                    m = modifiers.KEY_SHIFT;
                    break;
                case "ALT":
                    m = modifiers.KEY_ALT;
                    break;
                case "CTRL":
                    m = modifiers.KEY_CTRL;
                    break;
                default:
                    m = 0;
                    break;
            }
            return m;
        }

        private void carattere_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            if (e.Key == Key.LeftShift || e.Key == Key.RightShift) { 
                SHIFT.IsChecked= !SHIFT.IsChecked;
                return;
            }
            if(e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl)
            {
                CTRL.IsChecked = !CTRL.IsChecked;
                return;
            }
            if (e.Key == Key.LeftAlt || e.Key == Key.RightAlt || (e.Key == Key.System && e.SystemKey == Key.LeftAlt))
            {
                ALT.IsChecked = !ALT.IsChecked;
                return;
            }

            messageToSend.VK = (UInt16)KeyInterop.VirtualKeyFromKey(e.Key);

            // Change key name for numbers
            if ((e.Key >= Key.D0 && e.Key <= Key.D9))/* ||
                (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))*/
            {
                uint n = Convert.ToUInt32(e.Key - Key.D0);
                carattere.Text = Convert.ToString(n);
                return;
            }

            var strKey = new KeyConverter().ConvertToString(e.Key);

            // F10 is treated as system key
            if (e.Key == Key.System && e.SystemKey == Key.F10)
            {
                carattere.Text = "F10";
            }
            else if (e.Key >= Key.A && e.Key <= Key.Z)
            {
                carattere.Text = "KEY_" + strKey;
            }
            else
            {
                carattere.Text = strKey;
            }
        }

        private void sendCommandToAll()
        {
            Applicazione appSelected = (Applicazione)listViewApps.SelectedValue;

            if(appSelected == null)
            {
                MessageBox.Show(this, "Selezionare un'applicazione dall'elenco", "Selezionare un applicazione", MessageBoxButton.OK);
                return;
            }

            messageToSend.mods=ReadModifiers();
            foreach (RemoteServer rs in serverList)
            {
                foreach (Applicazione app in rs.applicationList)
                {
                    if (app.Process == appSelected.Process)
                    {
                        messageToSend.ksAppID = app.appID;
                        rs.sendCommand(messageToSend);
                    }
                }           
            }
        }
    }
    
}

