﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows;

namespace clientWPF
{
    public class ObservableConcurrentDictionary<TKey, TValue> : ConcurrentDictionary<TKey, TValue>, INotifyCollectionChanged
    {
        //public Dictionary<TKey, TValue> _base;
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public bool Add(TKey key, TValue value)
        {
            bool result = base.TryAdd(key, value);
            //base.Add(key, value);

            if (result)
            {
                KeyValuePair<TKey, TValue> added = new KeyValuePair<TKey, TValue>(key, value);
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, added));
            }
            else
                Console.WriteLine("Tentativo di inserimento in dizionario di chiave già esistente");
            return result;
        }

        public bool Remove(TKey key)
        {
            TValue value;
            bool result = base.TryRemove(key, out value);

            if (result)
            {
                KeyValuePair<TKey, TValue> removed = new KeyValuePair<TKey, TValue>(key, value);
                // TODO we need an index also if we want to send a Remove event instead of a reset...
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
            return result;
        }

        private void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            // Invoke the event *only* from the main UI thread
            Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                CollectionChanged?.Invoke(this, e);
            }));
        }
    }
}
