﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace clientWPF
{
    [StructLayout(LayoutKind.Sequential)]
    public struct BITMAPINFOHEADER
    {
        public uint biSize;
        public int biWidth;
        public int biHeight;
        public ushort biPlanes;
        public ushort biBitCount;
        public uint biCompression;
        public uint biSizeImage;
        public int biXPelsPerMeter;
        public int biYPelsPerMeter;
        public uint biClrUsed;
        public uint biClrImportant;

        public void Init()
        {
            biSize = (uint)Marshal.SizeOf(this);
        }
    }

    class Network
    {
        private RemoteServer remote;
        private TcpClient client;
        private NetworkStream stream;

        private byte[] headerBuffer;
        private byte[] messageBuffer;

        internal bool Connected {
            get {
                if (client != null)
                    return client.Connected;
                return false;
            }
        }

        public Network(RemoteServer remoteServer)
        {
            client = new TcpClient();
            //client.ExclusiveAddressUse = true;
            remote = remoteServer;
        }

        public void Close()
        {
            client?.Close();
            stream = null;
            client = null;
            Console.WriteLine("Comunicazione con il server terminata.");
        }

        public bool StartConnection(string hostname, string port)
        {
            // Connect to a remote device.
            try
            {
                // Connect the client to the remote endpoint. Catch any errors.
                client.Connect(hostname, int.Parse(port));

                stream = client.GetStream();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
            return true;
        }

        private static async void receiveCallback(IAsyncResult result)
        {
            // accedi alla classe in modo asincrono
            Network network = (Network)result.AsyncState;
            RemoteServer remote = network?.remote;

            if (remote == null || network?.stream == null)
                return;

            try
            {
                int headerSize = Marshal.SizeOf(typeof(message_header));
                int bytesRead = network.stream.EndRead(result);
                int offset = bytesRead;
                while (offset < headerSize)
                {
                    bytesRead = await network.stream.ReadAsync(network.headerBuffer, offset, headerSize - offset);
                    offset += bytesRead;
                }

                message_header msgHeader = readHeader(network.headerBuffer);
                switch (msgHeader.type)
                {
                    case message_type.SERVER_MSG_CREATE_APP:
                        // Ricevi dati messaggio
                        network.messageBuffer = new byte[msgHeader.msgSize];
                        offset = 0;
                        int tries = 3;
                        do
                        {
                            bytesRead = await network.stream.ReadAsync(network.messageBuffer, offset, (int)msgHeader.msgSize - offset);
                            if (bytesRead == 0)
                            {
                                // Nota: se bytesRead rimane 0 indefinitamente allora può crearsi un loop infinito! Aggiungere timeout o tentativi?
                                if (--tries <= 0)
                                    break;
                            }
                            offset += bytesRead;
                        }
                        while (offset < (int)msgHeader.msgSize);

                        if (offset != (int)msgHeader.msgSize)
                        {
                            throw new InvalidDataException("Unexpected header size.");
                        }
                        Applicazione app = readApp(msgHeader, network.messageBuffer);
                        remote.addApp(app);
                        break;
                    case message_type.SERVER_MSG_DELETE_APP:
                        remote.deleteApp(msgHeader.appID);
                        break;
                    case message_type.SERVER_MSG_FOCUS_APP:
                        remote.updateFocusApp(msgHeader.appID);
                        break;
                    case message_type.SERVER_MSG_LIST_APP:
                        // Non usato
                        break;
                    default:
                        break;
                }

                // Get next header message
                network.stream.BeginRead(network.headerBuffer, 0, Marshal.SizeOf(typeof(message_header)), new AsyncCallback(receiveCallback), network);
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (IOException)
            {
                // Server has probably closed the connection
                remote?.Close("connessione interrotta");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                remote?.Close("errore di comunicazione");
            }
        }

        internal void service()
        {
            int headerSize = Marshal.SizeOf(typeof(message_header));
            headerBuffer = new byte[headerSize];
            try
            {
                stream.BeginRead(headerBuffer, 0, headerSize, receiveCallback, this);
            }
            catch
            {
                remote.Close("connessione non riuscita.");
                Close();
            }
        }

        static private message_header readHeader(byte[] buffer)
        {
            // TODO check endiannes of struct?
            message_header msgHeader;
            int size = Marshal.SizeOf(typeof(message_header));
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.Copy(buffer, 0, ptr, size);
            msgHeader = (message_header) Marshal.PtrToStructure(ptr, typeof(message_header));
            Marshal.FreeHGlobal(ptr);
            return msgHeader;

            /*
            int index = 0;
            // Type
            msgHeader.type = (message_type)BitConverter.ToChar(buffer, index++);

            // Size/appID
            switch (msgHeader.type)
            {
                case message_type.SERVER_MSG_CREATE_APP:
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(buffer, index, Marshal.SizeOf(typeof(UInt64)));
                    msgHeader.msgSize = BitConverter.ToUInt64(buffer, index);
                    break;
                case message_type.SERVER_MSG_DELETE_APP:
                case message_type.SERVER_MSG_FOCUS_APP:
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(buffer, index, Marshal.SizeOf(typeof(UInt64)));
                    msgHeader.appID = BitConverter.ToUInt64(buffer, index);
                    break;
                default:
                    throw new InvalidDataException("Application data message parsing failed.");
                    //break;
            }
            
            return msgHeader;
            */
        }

        static private Applicazione readApp(message_header msgHeader, byte[] buffer)
        {
            try
            {
                int index = 0;
                // ID
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(buffer, index, Marshal.SizeOf(typeof(UInt64)));
                UInt64 appID = BitConverter.ToUInt64(buffer, index);
                index += Marshal.SizeOf(typeof(UInt64));

                // Title
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(buffer, index, Marshal.SizeOf(typeof(UInt32)));
                uint titleLen = BitConverter.ToUInt32(buffer, index);
                index += Marshal.SizeOf(typeof(UInt32));
                string title = readNBytes(buffer, titleLen, index);
                index += (int)titleLen;

                // Module Name
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(buffer, index, Marshal.SizeOf(typeof(UInt32)));
                uint moduleNameLength = BitConverter.ToUInt32(buffer, index);
                index += Marshal.SizeOf(typeof(UInt32));
                string moduleName = readNBytes(buffer, moduleNameLength, index);
                index += (int)moduleNameLength;

                BitmapSource bitmap = null;
                bool hasIcon = BitConverter.ToBoolean(buffer, index++);
                if (hasIcon)
                {
                    try
                    {
                        bitmap = readBitmap(buffer, ref index);
                    }
                    catch
                    {
                        throw new InvalidDataException("Error while reading bitmap data for the application.");
                    }
                }
                if (msgHeader.msgSize != (ulong)index)
                    throw new InvalidDataException("Application data message parsing failed.");

                Applicazione newApp = new Applicazione(appID, title, moduleName, bitmap);
                return newApp;
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e);
            }
            return null;
        }

        //metodo che legge len bytes dal buffer partendo dal byte in posizione startRead e ritorna il contenuto come stringa
        static private string readNBytes(byte[] buffer, uint len, int startRead)
        {
            try
            {
                int i, j = 0;
                byte[] temp = new byte[len];
                for (i = startRead; i < startRead + len; i++)
                {
                    temp[j] = buffer[i];
                    j++;
                }
                return Encoding.UTF8.GetString(temp);
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e);
            }
            return "";
        }

        static private BitmapSource readBitmap(byte[] buffer, ref int index)
        {
            // Read bitmap header
            int size = Marshal.SizeOf(typeof(BITMAPINFOHEADER));
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.Copy(buffer, index, ptr, size);
            BITMAPINFOHEADER header = (BITMAPINFOHEADER)Marshal.PtrToStructure(ptr, typeof(BITMAPINFOHEADER));
            Marshal.FreeHGlobal(ptr);
            index += size;

            byte[] bmpData = new byte[header.biSizeImage];
            Array.Copy(buffer, index, bmpData, 0, header.biSizeImage);
            index += (int)header.biSizeImage;

            // Check if top-down of bottom-up bitmap
            bool isTopDown = false;
            if (header.biHeight < 0)
            {
                // Top-down
                header.biHeight = -header.biHeight;
                isTopDown = true;
            }
            int stride = header.biWidth * ((int)(header.biBitCount + 7) / 8);
            if (!isTopDown)
            {
                // Flip data
                byte[] temp = new byte[stride];
                for (int row = 0; row < header.biHeight / 2; ++row)
                {
                    // Swap top with bottom
                    int offset = row * stride;
                    int reverseOffset = (header.biHeight - row - 1) * stride;
                    Array.Copy(bmpData, offset, temp, 0, stride);
                    Array.Copy(bmpData, reverseOffset, bmpData, offset, stride);
                    Array.Copy(temp, 0, bmpData, reverseOffset, stride);
                }
            }

            BitmapSource bitmap = BitmapSource.Create(
                header.biWidth,
                header.biHeight,
                0.0254 * header.biXPelsPerMeter,
                0.0254 * header.biYPelsPerMeter,
                PixelFormats.Bgra32,
                null,
                bmpData,
                stride);
            bitmap.Freeze();

            return bitmap;
        }

        internal void sendCommand(keystroke command)
        {
            message_header msg = new message_header();
            int size;
            msg.type = message_type.CLIENT_MSG_KEY;
            msg.key = command;
            byte[] msgInBytes = convertMessageInBytes(msg, out size);

            if (stream.CanWrite)
            {
                stream.Write(msgInBytes, 0, size);
            }
            else
            {
                Console.WriteLine("Non è possibile inviare il comando al server.");
            }
        }

        static private byte[] convertMessageInBytes(message_header msg, out int size)
        {
            // TODO check endiannes of struct?
            size = Marshal.SizeOf(msg);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(msg, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;

            /*
            int index = 0;
            int size = Marshal.SizeOf(msg);
            byte[] arr = new byte[size];
            BitConverter.GetBytes((byte)msg.type).CopyTo(arr, index++);
            BitConverter.GetBytes((UInt16)msg.key.VK).CopyTo(arr, index);
            index += 2;
            BitConverter.GetBytes((byte)msg.type).CopyTo(arr, index);
            index += 1;

            if (index != size)
                throw new Exception("Struct serialization ");
            */
        }
    }
}
