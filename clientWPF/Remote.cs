﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Timers;
using System.Windows;
using System.Windows.Media.Imaging;

namespace clientWPF
{
    public class Applicazione : INotifyPropertyChanged
    {
        internal UInt64 appID;
        private string title;
        private string moduleName;
        private BitmapSource bitmap;
        private double focusPercentage;
        internal double totalFocusTimeMS;

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public Applicazione(UInt64 appID, string title, string moduleName, BitmapSource bitmap)
        {
            focusPercentage = 0;
            totalFocusTimeMS = 0;

            this.appID = appID;
            this.title = title;
            this.moduleName = moduleName;
            if (bitmap != null)
            {
                this.bitmap = bitmap;
            }
        }

        public BitmapSource Icon { get { return bitmap; } }
        public string Process { get { return moduleName; } }
        public string Title { get { return title; } }
        public double FocusPct
        {
            get { return focusPercentage; }
            set
            {
                if (value != focusPercentage)
                {
                    focusPercentage = value;
                    NotifyPropertyChanged();
                }
            }
        }
    }

    public class RemoteServer : INotifyPropertyChanged
    {
        // TODO controllare accesso ai dati condivisi tra thread: socket, main, timer (elencoApp, focus, etc)!
        private Network connessione;
        private string IPaddress;
        private string port;
        private DateTime startTime = new DateTime();
        private DateTime lastFocusTimeUpdate = new DateTime();
        internal bool Connected { get { return connessione != null ? connessione.Connected : false; } }
        private Applicazione _appFocus = null;

        private object _lock = new object();

        private ObservableConcurrentDictionary<UInt64, Applicazione> applicationData = new ObservableConcurrentDictionary<UInt64, Applicazione>();
        internal ThreadSafeObservableCollection<Applicazione> applicationList = new ThreadSafeObservableCollection<Applicazione>();

        // View model
        public Applicazione AppFocus {
            get { return _appFocus; }
            set
            {
                if (_appFocus != value)
                {
                    _appFocus = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string ServerName { get { return IPaddress + " - " + port; } }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public event EventHandler<string> RemoteClosed;
        private void OnRemoteClose(string errorMsg)
        {
            // Call from main thread
            Application.Current.Dispatcher.Invoke(() => {
                RemoteClosed?.Invoke(this, errorMsg);
            });
        }

        public RemoteServer(string IP, string port)
        {
            this.IPaddress = IP;
            this.port = port;

            connessione = new Network(this /*, IP, port*/);

            if (connessione.StartConnection(IP, port))
            {
                //connessione andata a buon fine
                startTime = DateTime.Now;
                lastFocusTimeUpdate = startTime;

                // Start receiving data
                connessione.service();
            }
        }

        internal void Close(string errorMsg)
        {
            if (errorMsg != null)
                OnRemoteClose(errorMsg);
            connessione?.Close();
        }

        private void refreshFocusPercentage()
        {
            double deltaMS = DateTime.Now.Subtract(lastFocusTimeUpdate).TotalMilliseconds;
            double totalTime = DateTime.Now.Subtract(startTime).TotalMilliseconds;
            // Update AppFocus active time;
            if (AppFocus != null)
                AppFocus.totalFocusTimeMS += deltaMS;
            if (totalTime == 0)
                return;
            foreach (var entry in applicationList)
            {
                if (entry.totalFocusTimeMS != 0)
                {
                    entry.FocusPct = entry.totalFocusTimeMS / totalTime;
                }
            }
            lastFocusTimeUpdate = DateTime.Now;
        }

        internal void sendCommand(keystroke command)
        {
            connessione.sendCommand(command);
        }


        // IMPORTANT: This must do thread-safe operations!
        internal void addApp(Applicazione newApp)
        {
            lock (_lock)
            {
                if (newApp != null)
                {
                    applicationData.Add(newApp.appID, newApp);
                    applicationList.Add(newApp);
                }
            }
        }

        // IMPORTANT: This must do thread-safe operations!
        internal void deleteApp(UInt64 appId)
        {
            lock (_lock)
            {
                if (applicationData.ContainsKey(appId))
                {
                    Applicazione removed = applicationData[appId];
                    applicationData.Remove(appId);
                    applicationList.Remove(removed);
                }
                else
                {
                    Console.WriteLine("Cannot remove a not existing app.");
                }
            }
        }

        // IMPORTANT: This must do thread-safe operations!
        internal void updateFocusApp(UInt64 appID)
        {
            lock (_lock)
            {
                if (appID == 0)
                    return;

                // Refresh the focus time now to update the time of the current focus
                refreshFocusPercentage();

                Applicazione newFocus;
                if (applicationData.TryGetValue(appID, out newFocus))
                {
                    AppFocus = newFocus;
                }
                else
                {
                    AppFocus = null;
                    // Probabilmente l'id va ignorato se non esiste. Per ora, in debug lancia un'eccezione.
                    Console.WriteLine("ID focus non trovato tra le app create.");
                }
            }
        }

        // IMPORTANT: This must do thread-safe operations!
        internal void RefreshFocusPercentageEvent(object state, ElapsedEventArgs e)
        {
            lock (_lock)
            {
                refreshFocusPercentage();
            }
        }
    }
}