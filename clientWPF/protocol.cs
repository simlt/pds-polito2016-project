﻿using System;
using System.Runtime.InteropServices;

namespace clientWPF
{
    internal enum modifiers : Byte
    {
        KEY_CTRL = 0x1,
        KEY_ALT = 0x2,
        KEY_SHIFT = 0x4,
    };

    public enum message_type : Byte
    {
        SERVER_MSG_LIST_APP = 1,
        SERVER_MSG_CREATE_APP,
        SERVER_MSG_DELETE_APP,
        SERVER_MSG_FOCUS_APP,
        CLIENT_MSG_KEY,
    };

    [StructLayout(LayoutKind.Sequential, Pack=1)]
    public struct keystroke
    {
        internal UInt64 ksAppID;
        internal UInt16 VK;
        internal modifiers mods;
    };

    [StructLayout(LayoutKind.Explicit, Pack=1)]
    public struct message_header
    {
        [FieldOffset(0)] public message_type type;
        [FieldOffset(1)] public UInt64 appID;
        [FieldOffset(1)] public UInt64 msgSize;
        [FieldOffset(1)] public keystroke key;
    };
}
