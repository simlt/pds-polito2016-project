﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace clientWPF
{
    /// <summary>
    /// Logica di interazione per Window1.xaml
    /// </summary>
    public partial class WindowNewServer : Window
    {
        public WindowNewServer()
        {
            InitializeComponent();
        }

        private void buttonConnetti_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = (MainWindow)DataContext;
            if (main.addServer(textBoxAddress.Text, textBoxPort.Text))
            {
                main.listServer.SelectedIndex = main.listServer.Items.Count - 1;
                Close();
            }
        }
    }
}
