@echo off

call "D:\Programmi (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
IF NOT EXIST build MKDIR build
pushd build
set boostpath=D:\boost_1_61_0
set compileflags= -Zi -W4 -EHsc -I %boostpath%
cl %compileflags% -MD ..\src\server_main.cpp ..\src\server.cpp /link /INCREMENTAL:NO -LIBPATH:%boostpath%\stage\lib\ user32.lib shlwapi.lib gdi32.lib
cl %compileflags% -LD -MD -Feserverhook ..\src\server_hook.cpp /link /INCREMENTAL:NO -LIBPATH:%boostpath%\stage\lib\ user32.lib
popd
