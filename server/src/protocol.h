#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#include <stdint.h>


enum modifiers : uint8_t
{
	KEY_CTRL = 0x1,
	KEY_ALT = 0x2,
	KEY_SHIFT = 0x4,
};

#pragma pack(push, 1)
typedef struct
{
	uint64_t ksAppID;
	uint16_t VK;
	modifiers mods;
} keystroke;


enum message_type : uint8_t
{
	SERVER_MSG_LIST_APP = 1,
	SERVER_MSG_CREATE_APP,
	SERVER_MSG_DELETE_APP,
	SERVER_MSG_FOCUS_APP,

	CLIENT_MSG_KEY,
};

typedef struct
{
	message_type type;
	union
	{
		// Generic
		uint64_t uint64;
		// SERVER_MSG_FOCUS_APP
		// SERVER_MSG_DELETE_APP
		uint64_t appID; // HWND

		// SERVER_MSG_CREATE_APP
		uint64_t msgSize;

		// CLIENT_MSG_KEY
		keystroke key;
	};
} message_header;
#pragma pack(pop)

#endif // !PROTOCOL_H_