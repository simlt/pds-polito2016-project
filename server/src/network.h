#ifndef NETWORK_H_
#define NETWORK_H_


#include "protocol.h"
#include <thread>
#include <mutex>
#include <list>
#include <winsock2.h>

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN	512
#define MAX_BACKLOG_CONNECTIONS		0

class WindowMonitor;
class Application;

class ServerSocket
{
	WindowMonitor* monitorOwner;

	SOCKET listenSocket = INVALID_SOCKET;

	std::thread listenerThread;
	std::mutex mutex;

	// Shared variables (check thread-safety!)
	// volatile bool v_Ending = false;
	volatile SOCKET ts_ClientSocket = INVALID_SOCKET;
	std::list<keystroke> ts_keystrokeQueue;
	//

	SOCKET getClientSocket();
	void setClientSocket(SOCKET clientSocket);
	void closeClientSocket();

	void addKeystroke(keystroke key);

	void HandleClient();

	void SendClient(message_header msg);
	void SendClient(message_header msg, std::string& data);
public:
	ServerSocket(WindowMonitor* monitorOwner, int port);
	~ServerSocket();

	bool isValid() { return listenSocket != INVALID_SOCKET; }

	void SendClientCreateApp(Application& createApp);
	void SendClientRemoveApp(HWND deleteApp);
	void SendClientFocusApp(HWND focusApp);

	bool getNextKeystroke(keystroke& key);
};

#endif
