#define UNICODE		// Enable Unicode for window text
#include "server.h"
#include <string>
#include <vector>
#include <iostream>
#include <Psapi.h>
#include <Shlwapi.h>
#include <Shellapi.h>
#include <AppModel.h>


#define MAX_MESSAGES	100
#define BUFFLEN			300
#define SLEEP_TIME_MS	500

// Instantiation of static variable
bool WindowMonitor::isRunning;

typedef struct {
	HWND childHWnd;
	DWORD childpid;
} UWPInfo;

// Check if a child window has a different pid (for UWP apps)
BOOL CALLBACK EnumChildWindowsCallback(HWND hWnd, LPARAM lp) {
	wchar_t className[100];
	UWPInfo* info = (UWPInfo*)lp;
	if (GetClassName(hWnd, className, 100))
	{
		if (StrCmp(className, L"Windows.UI.Core.CoreWindow") == 0)
		{
			info->childHWnd = hWnd;
			GetWindowThreadProcessId(hWnd, &info->childpid);
			return FALSE;
		}
	}
	return TRUE;
}

class Application
{
	HWND windowHandle;
	DWORD processId;
	std::wstring title;
	std::wstring moduleName;
	bool hasIcon = false;
	BITMAPINFO bmpInfo = {};
	std::vector<BYTE> bmpData;

public:
	Application(HWND hwnd)
	{
		std::wstring fullModuleName;
		windowHandle = hwnd;
		moduleName = TEXT("UNK_MODULE");
		wchar_t strBuffer[MAX_PATH];

		// Do some tricks for UWP apps...
		UWPInfo info = { 0 };
		EnumChildWindows(hwnd, EnumChildWindowsCallback, (LPARAM)&info);
		if (info.childpid)
		{
			processId = info.childpid;
		}
		else
		{
			GetWindowThreadProcessId(hwnd, &processId);
		}

		// Find process name
		HANDLE processHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processId);
		if (processHandle)
		{
			// UWP app
			UINT32 bufferLength = 0;
			LONG result;
			if ((result = GetPackageId(processHandle, &bufferLength, nullptr)) == ERROR_SUCCESS)
			{
				BYTE* buffer = (PBYTE)malloc(bufferLength);
				result = GetPackageId(processHandle, &bufferLength, buffer);
				PACKAGE_ID* packageId = reinterpret_cast<PACKAGE_ID*>(buffer);
				packageId->name;
			}
			else if (GetModuleFileNameEx(processHandle, NULL, strBuffer, MAX_PATH))
			{
				fullModuleName = strBuffer;
				// Get only last part of filename (one past '\')
				moduleName = (StrRChr(strBuffer, NULL, TEXT('\\')) + 1);
			}
			else
			{
				OutputDebugString(TEXT("Failed getting module name\n"));
			}
			CloseHandle(processHandle);
		}
		else
		{
			OutputDebugString(TEXT("Failed getting module name\n"));
		}

		// Get icon handle
		// TODO What to do with Win10 apps? (Icon in AppStore)
		HICON iconHandle = 0;
		bool destroyIcon = false;
		// Get icon from module file
		if (ExtractIconEx(fullModuleName.c_str(), 0, &iconHandle, NULL, 1) > 0)
			destroyIcon = true;
		else
			iconHandle = (HICON)SendMessage(hwnd, WM_GETICON, ICON_BIG, 0);
		if (!iconHandle)
			iconHandle = (HICON)SendMessage(hwnd, WM_GETICON, ICON_SMALL, 0);
		if (!iconHandle)
			iconHandle = (HICON)SendMessage(hwnd, WM_GETICON, ICON_SMALL2, 0);
		if (!iconHandle)
			iconHandle = (HICON)GetClassLongPtr(hwnd, GCLP_HICON);
		if (!iconHandle)
			iconHandle = (HICON)GetClassLongPtr(hwnd, GCLP_HICONSM);
		/*if (!iconHandle)
			iconHandle = LoadIcon(NULL, IDI_APPLICATION);*/
		if (iconHandle)
		{
			// Get the bitmap data
			ICONINFO iconInfo;
			if (GetIconInfo(iconHandle, &iconInfo))
			{
				BITMAP bmp;
				HBITMAP bmpHandle = iconInfo.hbmColor;
				if (!bmpHandle)
				{
					bmpHandle = iconInfo.hbmMask;
					std::cout << "Warning: found a bitmap without color plane. This is not handled.\n";
				}

				if (bmpHandle)
				{
					if (GetObject(bmpHandle, sizeof(bmp), &bmp) > 0)
					{
						HDC hdc = GetDC(hwnd);
						bmpInfo = {};
						bmpInfo.bmiHeader.biSize = sizeof(bmpInfo.bmiHeader);

						// Get BITMAPINFO data
						if (GetDIBits(hdc, bmpHandle, 0, 0, NULL, &bmpInfo, DIB_RGB_COLORS))
						{
							bmpData.resize(bmpInfo.bmiHeader.biSizeImage);
							bmpInfo.bmiHeader.biCompression = BI_RGB;
							// Set negative height to get top-down DIB
							if (bmpInfo.bmiHeader.biHeight > 0)
							{
								bmpInfo.bmiHeader.biHeight = -bmpInfo.bmiHeader.biHeight;
							}

							// Get the bitmap data
							if (GetDIBits(hdc, bmpHandle, 0, bmpInfo.bmiHeader.biHeight, &bmpData[0], &bmpInfo, DIB_RGB_COLORS))
							{
								hasIcon = true;
							}
							else
							{
								OutputDebugString(TEXT("Failed getting bitmap pixel data\n"));
							}
						}
						else
						{
							OutputDebugString(TEXT("Failed getting BITMAPINFO data\n"));
						}
						ReleaseDC(hwnd, hdc);
					}
				}
				else
				{
					OutputDebugString(TEXT("Unable to find a valid handle for bitmap icon\n"));
				}
				// Release bitmaps
				if (iconInfo.hbmMask)
					DeleteObject(iconInfo.hbmMask);
				if (iconInfo.hbmColor)
					DeleteObject(iconInfo.hbmColor);
			}
			else
			{
				OutputDebugString(TEXT("Failed loading icon\n"));
			}
			if (destroyIcon)
			{
				DestroyIcon(iconHandle);
			}
		}

		updateAppInfo();
	}

	// Serialization
	friend std::ostream& operator<<(std::ostream& os, const Application& app)
	{
		char buffer[BUFFLEN];
		uint64_t ID = htonll(reinterpret_cast<uint64_t>(app.windowHandle));
		os.write(reinterpret_cast<const char*>(&ID), sizeof(ID));

		WideCharToMultiByte(CP_UTF8, 0, app.title.c_str(), -1, buffer, BUFFLEN, NULL, NULL);
		uint32_t titleLengthData = htonl((uint32_t)strlen(buffer));
		os.write(reinterpret_cast<const char*>(&titleLengthData), sizeof(titleLengthData));
		os.write(buffer, strlen(buffer));

		WideCharToMultiByte(CP_UTF8, 0, app.moduleName.c_str(), -1, buffer, BUFFLEN, NULL, NULL);
		uint32_t moduleNameLengthData = htonl((uint32_t)strlen(buffer));
		os.write(reinterpret_cast<const char*>(&moduleNameLengthData), sizeof(moduleNameLengthData));
		os.write(buffer, strlen(buffer));

		uint8_t hasIcon = app.hasIcon;
		os.write(reinterpret_cast<const char*>(&hasIcon), sizeof(hasIcon));
		if (hasIcon)
		{
			os.write(reinterpret_cast<const char*>(&app.bmpInfo.bmiHeader), sizeof(app.bmpInfo.bmiHeader));
			os.write(reinterpret_cast<const char*>(&app.bmpData[0]), app.bmpData.size());
		}
		return os;
	}

	void updateAppInfo()
	{
		wchar_t strBuffer[BUFFLEN];
		if (GetWindowText(windowHandle, strBuffer, BUFFLEN))
		{
			title = strBuffer;
		}
		else
		{
			title = TEXT("");
		}
	}

	void inputKeystroke(keystroke key)
	{
		WINDOWPLACEMENT wndpl;
		wndpl.length = sizeof(WINDOWPLACEMENT);
		// Get window minimized status
		if (GetWindowPlacement(windowHandle, &wndpl))
		{
			if (wndpl.showCmd == SW_SHOWMINIMIZED)
			{
				// Activate and show the window (if minimized)
				ShowWindow(windowHandle, SW_RESTORE);
			} 
			else
			{
				ShowWindow(windowHandle, SW_SHOW);
			}
		}

		//SwitchToThisWindow(windowHandle, FALSE);
		if (!SetForegroundWindow(windowHandle))
		{
			OutputDebugString(TEXT("Unable to bring app to foreground.\n"));
			return;
		}

		// Wait for the window to be ready for input
		Sleep(250);

		INPUT input;
		input.type = INPUT_KEYBOARD;
		input.ki.wScan = 0;
		input.ki.dwFlags = 0;
		input.ki.time = 0;
		input.ki.dwExtraInfo = 0;

		// Press mod keys
		if (key.mods & KEY_CTRL)
		{
			input.ki.wVk = VK_CONTROL;
			SendInput(1, &input, sizeof(INPUT));
		}
		if (key.mods & KEY_ALT)
		{
			input.ki.wVk = VK_MENU;
			SendInput(1, &input, sizeof(INPUT));
		}
		if (key.mods & KEY_SHIFT)
		{
			input.ki.wVk = VK_SHIFT;
			SendInput(1, &input, sizeof(INPUT));
		}
		// Send actual key
		input.ki.wVk = key.VK;
		SendInput(1, &input, sizeof(INPUT));
		// Release all keys
		input.ki.dwFlags = KEYEVENTF_KEYUP;

		SendInput(1, &input, sizeof(INPUT));
		if (key.mods & KEY_CTRL)
		{
			input.ki.wVk = VK_CONTROL;
			SendInput(1, &input, sizeof(INPUT));
		}
		if (key.mods & KEY_ALT)
		{
			input.ki.wVk = VK_MENU;
			SendInput(1, &input, sizeof(INPUT));
		}
		if (key.mods & KEY_SHIFT)
		{
			input.ki.wVk = VK_SHIFT;
			SendInput(1, &input, sizeof(INPUT));
		}
	}

	void print()
	{
		std::cout << windowHandle << " - " << getProcessId() << ": ";
		std::wstring str = getTitle() + L" (" + getModuleName() + L")\n";
		// Use WriteConsole to handle correctly UNICODE wchars
		WriteConsoleW(GetStdHandle(STD_OUTPUT_HANDLE), str.c_str(), str.size(), nullptr, nullptr);
	}

	const std::wstring getTitle() { return title; }
	const std::wstring getModuleName() { return moduleName; }
	const DWORD getProcessId() { return processId; }
};


BOOL CALLBACK EnumCallback(HWND hwnd, LPARAM lParam)
{
	WindowMonitor* monitor = (WindowMonitor*)lParam;
	if (IsWindowVisible(hwnd))
	{
		monitor->addAppIfNew(hwnd);
	}
	return TRUE;
}

WindowMonitor::WindowMonitor(int port)
	: serverSocket(this, port)
{
	// Initialize windows list
	refreshWindowList();
}

WindowMonitor::~WindowMonitor()
{
}

void WindowMonitor::run()
{
	// Check if network is correctly set-up
	if (!serverSocket.isValid())
	{
		std::cout << "Server socket could not start with success.\nReturn to quit...";
		std::cin.get();
		return;
	}

	//printAll();
	//printActive();

	// Start monitor loop
	isRunning = true;
	while (isRunning)
	{
		// Use polling instead of message events to check for new windows
		refreshWindowList();

		// Update active app
		updateActiveApp();

		// Check if any remote command has been received from client
		keystroke key;
		while (serverSocket.getNextKeystroke(key))
		{
			sendKeystrokeToApp(key);
			std::cout << "Received keystroke input from client for ("  << (HWND)key.ksAppID << "): " << key.VK << " mods: " << std::hex << key.mods << std::endl;
		}

		// sleep
		Sleep(SLEEP_TIME_MS);
	}
}

void WindowMonitor::stop()
{
	isRunning = false;
}


void WindowMonitor::refreshWindowList()
{
	// Search for deleted windows before refresh
	auto appItr = apps.cbegin();
	while (appItr != apps.cend())
	{
		HWND hWnd = appItr->first;
		++appItr; // Must iterate on next before erase operation
		if (!IsWindow(hWnd) || !IsWindowVisible(hWnd))
		{
			/*char str[100];
			wsprintf(str, "Deleted app %x\n", appItr->first);
			OutputDebugString(str);*/
			removeApp(hWnd);
		}
	}
	// Check for new windows
	EnumWindows(&EnumCallback, (LPARAM)this);
}

Application& WindowMonitor::addAppIfNew(HWND hwnd)
{
	// Get last active window of topmost window passed. It may return the same handle
	/*HWND lastHwnd = GetLastActivePopup(hwnd);
	if (lastHwnd)
		hwnd = lastHwnd;*/

	// check if app doesn't exist
	auto itr = apps.find(hwnd);
	if (itr == apps.end())
	{
		auto result = apps.emplace(std::piecewise_construct,
			std::forward_as_tuple(hwnd),
			std::forward_as_tuple(Application(hwnd)));
		// Check if insert was successful
		if (result.second)
		{
			itr = result.first;
			serverSocket.SendClientCreateApp(itr->second);
		}
	}
	return itr->second;
}

void WindowMonitor::removeApp(HWND hwnd)
{
	if (hwnd)
	{
		apps.erase(hwnd);
		serverSocket.SendClientRemoveApp(hwnd);
	}
}

void WindowMonitor::updateActiveApp(bool forceSend)
{
	bool appChanged = false;
	// Update active app
	HWND currentHwnd = GetForegroundWindow();
	// Do some tricks for UWP apps...
	//DWORD childpid = 0;
	//EnumChildWindows(currentHwnd, EnumChildWindowsCallback, (LPARAM)&childpid);
	if (currentHwnd && activeApp != currentHwnd)
	{
		activeApp = currentHwnd;
		appChanged = true;
		Application& app = addAppIfNew(currentHwnd);
		app.updateAppInfo();
		printActive();
	}
	// Send client focus notification if app changed or forceSend is set from initClient()
	if (appChanged || forceSend)
	{
		serverSocket.SendClientFocusApp(currentHwnd);
	}
}

Application& WindowMonitor::getActiveApp()
{
	updateActiveApp();
	return apps.at(activeApp); // This should ALWAYS be found
}

void WindowMonitor::sendKeystrokeToApp(keystroke key)
{
	HWND hwnd;
	if (key.ksAppID == INPUT_FOCUS_APP)
	{
		hwnd = activeApp;
	}
	else
	{
		hwnd = reinterpret_cast<HWND>(key.ksAppID);
	}

	auto itr = apps.find(hwnd);
	if (itr != apps.end())
	{
		itr->second.inputKeystroke(key);
	}
	else
	{
		OutputDebugString(TEXT("Invalid HWND provided for send command.\n"));
	}
}

void WindowMonitor::initClientConnection()
{
	std::cout << "New client connection\n";
	for (auto &appItr : apps)
	{
		serverSocket.SendClientCreateApp(appItr.second);
	}
	updateActiveApp(true);
}

void WindowMonitor::printAll()
{
	for (auto appItr = apps.begin(); appItr != apps.end(); ++appItr)
	{
		appItr->second.print();
	}
}

void WindowMonitor::printApp(HWND hwnd)
{
	auto itr = apps.find(hwnd);
	if (itr != apps.end())
		itr->second.print();
}

void WindowMonitor::printActive()
{
	std::cout << "[CurrentAPP]:\n ";
	printApp(activeApp);
}
