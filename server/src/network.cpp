#include "network.h"
#include "server.h"
#include <sstream>
#include <WS2tcpip.h>

ServerSocket::ServerSocket(WindowMonitor* monitorOwner, int port)
	: monitorOwner(monitorOwner)
{
	int iResult;

	// Initialize Winsock
	WSADATA wsaData;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		return;
	}

	struct addrinfo *result = NULL, *ptr = NULL, hints;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the local address and port to be used by the server
	iResult = getaddrinfo(NULL, std::to_string(port).c_str(), &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed: %d\n", iResult);
		WSACleanup();
		return;
	}

	// Create a SOCKET for the server to listen for client connections
	SOCKET localListenSocket;
	localListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

	if (localListenSocket == INVALID_SOCKET) {
		printf("Error at socket(): %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return;
	}

	// Setup the TCP listening socket
	iResult = bind(localListenSocket, result->ai_addr, (int)result->ai_addrlen);
	freeaddrinfo(result);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		closesocket(localListenSocket);
		WSACleanup();
		return;
	}

	if (listen(localListenSocket, MAX_BACKLOG_CONNECTIONS) == SOCKET_ERROR) {
		printf("Listen failed with error: %ld\n", WSAGetLastError());
		closesocket(localListenSocket);
		WSACleanup();
		return;
	}

	listenSocket = localListenSocket;

	// Start listen thread and move on (asynchronous handling)
	listenerThread = std::thread(&ServerSocket::HandleClient, this);

	printf("Server successfully started on port: %d.\n", port);
}

ServerSocket::~ServerSocket()
{
	closesocket(listenSocket);
	closeClientSocket();
	WSACleanup();
	// Join with thread (which should return after closing the socket)
	if (listenerThread.joinable())
	{
		listenerThread.join();
	}
}

SOCKET ServerSocket::getClientSocket()
{
	std::lock_guard<std::mutex> lock(mutex);
	SOCKET client = ts_ClientSocket;
	return client;
}

void ServerSocket::setClientSocket(SOCKET clientSocket)
{
	std::lock_guard<std::mutex> lock(mutex);
	ts_ClientSocket = clientSocket;
}

void ServerSocket::closeClientSocket()
{
	std::lock_guard<std::mutex> lock(mutex);
	closesocket(ts_ClientSocket);
	ts_ClientSocket = INVALID_SOCKET;
}

// NOTE: This is run on a separate thread. Make sure it's thread safe
void ServerSocket::HandleClient()
{
	int iResult;
	char recvbuf[sizeof(message_header)];
	// Keep a local non shared copy of clientSocket
	SOCKET localClientSocket = INVALID_SOCKET;

	for (;;)
	{
		// Accept a client socket (this will also return when ListenSocket is closed)
		localClientSocket = accept(listenSocket, NULL, NULL);
		if (localClientSocket == INVALID_SOCKET) {
			printf("accept failed: %d\n", WSAGetLastError());
			closesocket(listenSocket);
			listenSocket = INVALID_SOCKET;
			WSACleanup();
			return;
		}
		// Update clientSocket
		setClientSocket(localClientSocket);

		// Send initial data to client
		monitorOwner->initClientConnection();

		// Serve the only connected client until it disconnects
		int offset = 0;
		do
		{
			int missing = sizeof(message_header) - offset;
			iResult = recv(localClientSocket, recvbuf + offset, missing, 0);
			if (iResult > 0)
			{
				offset += iResult;
				printf("Bytes received: %d\n", iResult);
				if (offset >= sizeof(message_header))
				{
					message_header msg = *((message_header*)recvbuf);
					if (msg.type == CLIENT_MSG_KEY)
					{
						addKeystroke(msg.key);
					}
					else
					{
						OutputDebugString("Unhandled CLIENT_MSG received from client.\n");
					}
					offset = 0;
				}
			}
			else
			{
				printf("Connection closed.\n");
				closeClientSocket();
			}
		} while (iResult > 0);
	}
}

void ServerSocket::SendClientCreateApp(Application& createApp)
{
	if (getClientSocket() == INVALID_SOCKET)
		return;

	message_header msg;
	msg.type = SERVER_MSG_CREATE_APP;

	std::ostringstream streambuf;
	streambuf << createApp;
	std::string& buffer(streambuf.str());
	msg.msgSize = buffer.size();

	SendClient(msg, buffer);
}

void ServerSocket::SendClientRemoveApp(HWND deleteApp)
{
	if (getClientSocket() == INVALID_SOCKET)
		return;

	message_header msg;
	msg.type = SERVER_MSG_DELETE_APP;
	msg.appID = reinterpret_cast<uint64_t>(deleteApp);
	SendClient(msg);
}

void ServerSocket::SendClientFocusApp(HWND focusApp)
{
	if (getClientSocket() == INVALID_SOCKET)
		return;

	message_header msg;
	msg.type = SERVER_MSG_FOCUS_APP;
	msg.appID = reinterpret_cast<uint64_t>(focusApp);
	SendClient(msg);
}

void ServerSocket::SendClient(message_header msg)
{
	/*int iResult;
	iResult = send(getClientSocket(), reinterpret_cast<const char*>(&msg), sizeof(msg), 0);
	if (iResult == SOCKET_ERROR) {
		wprintf(L"send failed with error: %d\n", WSAGetLastError());
		closeClientSocket();
	}

	printf("Bytes Sent: %d\n", iResult);*/
	std::string data = "";
	SendClient(msg, data);
}

void ServerSocket::SendClient(message_header msg, std::string& data)
{
	int iResult;
	std::ostringstream sendbuf;
	// Send header data
	sendbuf.write(reinterpret_cast<const char*>(&msg), sizeof(msg));
	// Send actual data
	sendbuf << data;

	std::string buffer(sendbuf.str());
	iResult = send(getClientSocket(), buffer.c_str(), (int)buffer.size(), 0);
	if (iResult == SOCKET_ERROR) {
		printf("Connection closed by client.\n");
		closeClientSocket();
		return;
	}

	//printf("Bytes Sent: %d\n", iResult);
}


void ServerSocket::addKeystroke(keystroke key)
{
	std::lock_guard<std::mutex> lock(mutex);
	ts_keystrokeQueue.push_back(key);
}

bool ServerSocket::getNextKeystroke(keystroke& key)
{
	std::lock_guard<std::mutex> lock(mutex);
	if (ts_keystrokeQueue.empty())
	{
		return false;
	}
	key = ts_keystrokeQueue.front();
	ts_keystrokeQueue.pop_front();
	return true;
}
