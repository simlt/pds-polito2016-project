#include "server.h"
#include <iostream>
#include <string>

BOOL WINAPI CtrlHandlerRoutine(DWORD dwCtrlType)
{
	if (dwCtrlType == CTRL_C_EVENT)
	{
		std::cout << "[Ctrl+C] Exiting...\n";
		WindowMonitor::isRunning = false;
		// Signal is handled - don't pass it on to the next handler
		return TRUE;
	}
	else
	{
		// Pass signal on to the next handler
		return FALSE;
	}
}

int main(int argc, char* argv[])
{
	// Get port
	int port = DEFAULT_PORT;
	if (argc > 1)
	{
		try
		{
			port = std::stoi(argv[1]);
		}
		catch (std::exception e)
		{
			std::cout << "Invalid argument for port. Using default port (" << port << ")\n";
		}
	}

	// Initialize monitor
	WindowMonitor monitor(port);

	// Handle CTRL-C to stop
	SetConsoleCtrlHandler(CtrlHandlerRoutine, TRUE);

	monitor.run();

	return 0;
}
