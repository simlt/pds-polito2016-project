#ifndef SERVER_H_
#define SERVER_H_

// This define is needed in case we include also "windows.h" before including "winsock2.h"
#define WIN32_LEAN_AND_MEAN

#include "network.h"
#include <Windows.h>
#include <map>

#define DEFAULT_PORT	1234
#define INPUT_FOCUS_APP		0

class Application;
extern std::ostream& operator<<(std::ostream& os, const Application& app);

class WindowMonitor
{
	std::map<HWND, Application> apps;
	HWND activeApp = nullptr;

	ServerSocket serverSocket;

public:
	WindowMonitor(int port);
	~WindowMonitor();

	static bool isRunning;
	void run();
	void stop();

	void refreshWindowList();
	void updateActiveApp(bool forceSend = false);
	Application& getActiveApp();
	Application& addAppIfNew(HWND hwnd);
	void removeApp(HWND hwnd);
	void sendKeystrokeToApp(keystroke key);

	void initClientConnection();

	void printAll();
	void printApp(HWND hwnd);
	void printActive();
};



#endif /* end of include guard: SERVER_H_ */
