# Project for the System Programming 2015/2016 course of Computer Engineering master degree.

## Development of a client-server solution for remote control of one or more Windows PCs.
## Sviluppo di una soluzione client-server per il controllo remoto di uno o più computer Windows.

### Subdirectories:
 - server: monitors the active processes on the host system
 - clientWPF: connects to the server and displays active processes and it can send commands